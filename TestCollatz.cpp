// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);}

//my tests
TEST(CollazFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(789, 850));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 789);
    ASSERT_EQ(j, 850);
    ASSERT_EQ(v, 135);
}

TEST(CollazFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(100000, 100020));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100000);
    ASSERT_EQ(j, 100020);
    ASSERT_EQ(v, 310);
}

TEST(CollazFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(543597, 543610));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 543597);
    ASSERT_EQ(j, 543610);
    ASSERT_EQ(v, 209);
}


TEST(CollazFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(700000, 699990));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 700000);
    ASSERT_EQ(j, 699990);
    ASSERT_EQ(v, 230);
}

TEST(CollazFixture, eval8) {
    tuple<int, int, int> t = collatz_eval(make_pair(34569, 34569));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 34569);
    ASSERT_EQ(j, 34569);
    ASSERT_EQ(v, 161);
}

TEST(CollazFixture, eval9) {
    tuple<int, int, int> t = collatz_eval(make_pair(995000, 995015));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 995000);
    ASSERT_EQ(j, 995015);
    ASSERT_EQ(v, 228);
}

TEST(CollazFixture, eval10) {
    tuple<int, int, int> t = collatz_eval(make_pair(150, 149));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 150);
    ASSERT_EQ(j, 149);
    ASSERT_EQ(v, 24);
}

TEST(CollazFixture, eval11) {
    tuple<int, int, int> t = collatz_eval(make_pair(8324, 8567));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 8324);
    ASSERT_EQ(j, 8567);
    ASSERT_EQ(v, 234);
}

TEST(CollazFixture, eval12) {
    tuple<int, int, int> t = collatz_eval(make_pair(234, 250));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 234);
    ASSERT_EQ(j, 250);
    ASSERT_EQ(v, 128);
}

TEST(CollazFixture, eval13) {
    tuple<int, int, int> t = collatz_eval(make_pair(25398, 25390));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 25398);
    ASSERT_EQ(j, 25390);
    ASSERT_EQ(v, 202);
}

TEST(CollazFixture, eval14) {
    tuple<int, int, int> t = collatz_eval(make_pair(9054, 9063));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 9054);
    ASSERT_EQ(j, 9063);
    ASSERT_EQ(v, 92);
}

TEST(CollazFixture, eval15) {
    tuple<int, int, int> t = collatz_eval(make_pair(999999, 999999));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 999999);
    ASSERT_EQ(j, 999999);
    ASSERT_EQ(v, 259);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n789 850\n100000 100020\n543597 543610\n700000 699990\n34569 34569\n995000 995015\n150 149\n8324 8567\n234 250\n25398 25390\n9054 9063\n999999 999999\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n789 850 135\n100000 100020 310\n543597 543610 209\n700000 699990 230\n34569 34569 161\n995000 995015 228\n150 149 24\n8324 8567 234\n234 250 128\n25398 25390 202\n9054 9063 92\n999999 999999 259\n", oss.str());}
