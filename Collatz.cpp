// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;
// ------------
// collatz_read
// ------------
int arrayLength = 500000;
int sequences [500000];

pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

//recursive helper for collatz length
int findSequence(long current) {
    if(current == 1) {
        return 1;
    } else if(current < arrayLength && sequences[current] != 0) {
        return sequences[current];
    } else if(current % 2 == 0) {
        return 1 + findSequence(current / 2);
    } else {
        //optimization from quiz
        return 1 + findSequence(current * 3 + 1);
    }
}

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    int high;
    int low;
    tie(i, j) = p;
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);
    int highest = 1;
    if(i > j) {
        high = i;
        low = j;
    } else {
        high = j;
        low = i;
    }
    //loop through numbers
    for(int cur = low; cur <= high; ++cur) {
        int length;
        if(cur < arrayLength && sequences[cur] != 0) {
            length = sequences[cur];
        } else {
            length = findSequence(cur);
            if(cur < arrayLength) {
                sequences[cur] = length;
            }
        }
        if(length > highest) {
            highest = length;
        }
    }
    assert(highest > 0);
    return make_tuple(i, j, highest);
}



// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    //intialize all array values to 0
    for(int i = 0; i < arrayLength; ++i) {
        sequences[i] = 0;
    }
    //find first 100 numbers
    for(int i = 1; i <= 100; ++i) {
        int length = 1;
        long current = i;
        while(current != 1) {
            if(current % 2 == 0) {
                current = current / 2;
            } else {
                current = current * 3 + 1;
            }
            length++;
        }
        sequences[i] = length;
    }
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}
